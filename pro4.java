/*
PROGRAM 4 : Write a Program to find min and max element in array
*/

import java.io.*;
public class maxDemo {
    public static void main(String[] args) throws IOException{
        BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
	
        System.out.println("Enter Dimensions");
         int size= Integer.parseInt(br.readLine());

         int adii[] = new int[size];

         System.out.println("Enter Array Elements");
       
       	 for(int i=0; i<adii.length; i++){
            adii[i] = Integer.parseInt(br.readLine());
         }

         int x= adii[0];

        for(int i=1; i<adii.length; i++){
            if(x<adii[i]){
                x=adii[i];
            }
        }
        System.out.println("Max="+x);

        x = adii[0];
        for(int i=1; i<adii.length; i++){
            if(x>adii[i]){
                x=adii[i];
            }
        }
        System.out.println("Min="+x);

    }
}

